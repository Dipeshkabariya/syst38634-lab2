package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		
		assertFalse("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		
		assertTrue("The time provided does not match the result", totalSeconds == 3719);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
	}
	
	
	@Test
	public void testGetTotalMilliSeconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invaild number of milliseconds",totalMilliseconds == 5	);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilliSecondsExceptions() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invaild number of milliseconds");
	}
	

	@Test
	public void testGetTotalMilliSecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invaild number of milliseconds",totalMilliseconds == 999 );
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilliSecondBoundaryOut() {
		int totalMillisecond = Time.getTotalMilliseconds("12:05:50:1000");
	 assertTrue("Invaild number of millisecond" , totalMillisecond == 1000);
	}
}


